<?php
    require_once('animal.php');
    require_once('frog.php');
    require_once('ape.php');

    $sheep = new Animal("shaun");
    echo "Nama : ". $sheep->name."<br>";
    echo "Kaki : ". $sheep->legs."<br>";
    echo "Cold Blooded : ". $sheep->cold_blooded."<br><br>";

    $kodok = new Frog("buduk");
    echo "Nama : ". $kodok->name."<br>";
    echo "Kaki : ". $kodok->legs."<br>";
    echo "Cold Blooded : ". $kodok->cold_blooded."<br>";
    echo "Jump : ". $kodok->jump."<br><br>";

    $monyet = new Ape("kera sakti");
    echo "Nama : ". $monyet->name."<br>";
    echo "Kaki : ". $monyet->legs."<br>";
    echo "Cold Blooded : ". $monyet->cold_blooded."<br>";
    echo "Yell : ". $monyet->yell."<br>";
?>